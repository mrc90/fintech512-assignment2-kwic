import java.util.*;

public class kwic {
    public static void main(String [] args) {

        // Read line(s) from stdin.  There are 2 parts, separated by a line "::".
        // The first part is a list of words to ignore, 1 word per line.
        // Read each word, and force to lower case.
        // Put each word-to-ignore into a set of words to ignore, to make it easy to lookup a word.

        Set<String> ignoreWords = new HashSet<>();
        Scanner input = new Scanner(System.in);
        while (input.hasNext()) {
            String line = input.nextLine();
            if (line.equals("::")) {
                break;
            }
            line = line.toLowerCase();
            ignoreWords.add(line);
        }

        // The second parts is a list of titles, 1 title per line, each a list of 1 or more words.
        // Need to split the title into a list of words, lowercase.
        // Add the title (list of words) to an array of titles; the index is the title's id.
        // Then for each word, add it to a map of word-to-list-of-title-id.  Append the title's id to the list-of-title-id.

        Map<String, ArrayList<Integer>> wordToTitles = new HashMap<>();
        ArrayList<String[]> titles = new ArrayList<>();
        while (input.hasNext()) {
            String line = input.nextLine();
            line = line.toLowerCase();
            String[] words = line.split(" ");
            for (String word: words) {
                if (!ignoreWords.contains(word)) {
                    if (!wordToTitles.containsKey(word)) {
                        wordToTitles.put(word, new ArrayList<>());
                    }
                    wordToTitles.get(word).add(titles.size());
                }
            }
            titles.add(words);
        }

        // Input has all been read!
        // Walk the words (keys) in the map of word-to-list-of-title-id, in lexical order.
        // For each word, walk each title id in the (value) list-of-title-id, remembering a count of how many times we've seen this title id for this word.
        // Walk each word in the title.  If the word is the same as the (key), *and* the occurance of the word == the count from above), print it uppercase (*).  Otherwise, print it lowercase.

        String[] keyWords = wordToTitles.keySet().toArray(new String[0]);
        Arrays.sort(keyWords);
        for (String keyWord: keyWords) {
            int lastId = -1;
            int nthTimeTitleSeen = 0;
            for (int id: wordToTitles.get(keyWord)) {
                if (id != lastId) {
                    nthTimeTitleSeen = 0;
                } else {
                    nthTimeTitleSeen = nthTimeTitleSeen + 1;
                }
                lastId = id;
                int nthTimeWordSeen = 0;
                for (String word: titles.get(id)) {
                    if (word.equals(keyWord)) {
                        if (nthTimeWordSeen==nthTimeTitleSeen) {
                            System.out.print(" " + word.toUpperCase());
                        } else {
                            System.out.print(" " + word);
                        }
                        nthTimeWordSeen += 1;
                    } else {
                        System.out.print(" " + word);
                    }
                }
                System.out.println();
            }
        }
    }
}
